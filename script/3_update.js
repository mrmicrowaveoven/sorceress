function update () {
  cursors = this.input.keyboard.createCursorKeys();
  var spaceBar = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
  if (cursors.left.isDown)
  {
      player.setVelocityX(-160);
      player.flipX = true
      player.anims.play('left', true);
  }
  else if (cursors.right.isDown)
  {
      player.setVelocityX(160);
      player.flipX = false
      player.anims.play('right', true);
  }
  else if (spaceBar.isDown) {
    player.anims.play('cast', true)
  }
  else
  {
      player.setVelocityX(0);

      player.anims.play('turn');
  }

  if (cursors.up.isDown && player.body.touching.down)
  {
      player.setVelocityY(-500);
  }
}
